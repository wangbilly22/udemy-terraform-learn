terraform {
  required_version = ">= 0.12"
  backend "s3" {
    # bucket = "myapp-bucket"
    bucket = "billy-learn-myapp-bucket"
    key    = "myapp/state.tfstate"
    region = "ap-southeast-1"
  }
}

provider "aws" {
  region = "ap-southeast-1"
  # access_key = "AKIASNIMIDTZIUZPYRDV"
  # secret_key = "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"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "my-vpc"
  cidr = var.vpc_cidr_block

  # azs             = ["eu-west-1a", "eu-west-1b", "eu-west-1c"]
  azs = [var.avail_zone]
  # private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets = [var.subnet_cidr_block]
  public_subnet_tags = {
    Name = "${var.env_prefix}-subnet-1"
  }

  # enable_nat_gateway = true
  # enable_vpn_gateway = true

  tags = {
    # Terraform   = "true"
    # Environment = "dev"
    Name = "${var.env_prefix}-vpc"
  }
}

module "myapp-webserver" {
  source = "./modules/webserver"
  # vpc_id              = aws_vpc.myapp-vpc.id
  vpc_id              = module.vpc.vpc_id
  my_ip               = var.my_ip
  env_prefix          = var.env_prefix
  image_name          = var.image_name
  public_key_location = var.public_key_location
  instance_type       = var.instance_type
  # subnet_id           = module.myapp-subnet.subnet.id
  subnet_id  = module.vpc.public_subnets[0]
  avail_zone = var.avail_zone
}

#!/bin/bash
# sudo yum update -y && sudo yum install -y docker
# sudo systemctl start docker 
# sudo usermod -aG docker ec2-user
# docker run -p 8080:80 nginx

#!/bin/bash
sudo yum update -y && sudo yum install -y docker
sudo systemctl start docker
 
#Add ec2-user to the group "docker" so that ec2-user can run docker without sudo.
sudo usermod -aG docker ec2-user
 
# Modify acl for the unix docker socket to be r/w by ec2-user.
sudo setfacl -m user:ec2-user:rw /var/run/docker.sock
 
# run docker with "-d" to set it run as background job, otherwise the provisioner "remote-exec" will not exit as long as docker is running.
 
docker run -d --name "myapp-nginx-docker" -p 8080:80 nginx
exit 0


output "aws_ami_id" {
  # value = moduledata.aws_ami.latest-amazon-linux-image.id
  value = module.myapp-webserver.ami_id
}

output "ec2_public_ip" {
  # value = aws_instance.myapp-server.public_ip
  value = module.myapp-webserver.instance.public_ip
}